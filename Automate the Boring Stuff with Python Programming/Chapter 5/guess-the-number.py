'''
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1
    Description: Developed game with all the concepts before learnt.
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

# this is a guess the number name.
import random

print('Hello, what is your name?')
name = input()

print('Well, ' +name+ ', I\m thinking a number between 1 and 20.')
secretNumber = random.randint(1, 20)

for guessesTaken in range(1, 7):
    print('Take a guess.')
    try:
        guess = int(input())

        if guess < secretNumber:
            print('Your guess is too low')
        elif guess > secretNumber:
            print('Your guess is too high')
        else:
            break # This condition is for the correct guess.
    except ValueError as ve:
        print('You did not enter a number. Try again...')
        continue

if guess == secretNumber:
    print('Good job, ' +name+ '! You guessed my number in ' +str(guessesTaken)+ ' guesses.')
else:
    print('Nope. The number I was thinking of was ' +str(secretNumber)+ '.')
