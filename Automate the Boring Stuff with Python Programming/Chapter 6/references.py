"""
    Date created: 14/08/2017
    Date last modified: 14/08/2017
    Python version: 3.6.1

    Description: Showing how references in Python works.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"

import copy

spam = 42                           # assign a integer value
cheese = spam                       # Copying the VALUE of spam variable to cheese variable
spam = 100                          # Assign new integer value

print('SPAM: ' + str(spam))         # It'll print the most recent value attached to spam
print('CHEESE: ' + str(cheese))     # Here, Will be printed the VALUE of cheese variable, the old one value from spam.



#   So, List do not works that way, when you assign a list to variable you are actually assigning a reference of that list
spam = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
cheese = spam
cheese2 = list(spam)                # Here, we can "avoid" this behavior doing a "new copy" from this list in other memory reference value.
cheese3 = copy.deepcopy(spam)       # Second way to do same thing - 
spam[3] = 'TESTE'

print('SPAM: ' + str(spam))
print('CHEESE: ' + str(cheese))
print('CHEESE2: ' + str(cheese2))
print('CHEESE3: ' + str(cheese3))
