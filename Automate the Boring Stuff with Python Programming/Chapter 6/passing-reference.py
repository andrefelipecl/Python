"""
    Date created: 14/08/2017
    Date last modified: 14/08/2017
    Python version: 3.6.1

    Description: Showing how references in Python works.
                 References are particularly important for understanding how arguments get passed to functions. 
                 When a function is called, the values of the arguments are copied to the parameter variables.
"""

def eggs(someParameter):
    someParameter.append('Hello')

spam = [1, 2, 3]
eggs(spam)
print(spam)
