"""
    Date created: 14/08/2017
    Date last modified: 14/08/2017
    Python version: 3.6.1

    Description: Showing some similarities between lists and strings in Python.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"

# if you consider a string to be a “list” of single text characters. Many of the things you can do 
# with lists can also be done with strings: indexing; slicing; and using them with for loops, with 
# len(), and with the in and not in operators.
name = 'Zophie'
name[0]
name[-2]
name[0:4]

print('Zo' in name)
print('z' in name)
print('p' not in name)

# We cannot assign a new value in a String, because a string are imutable when accessed by indexes:
# For Example:
#       name = 'Zophie a cat'
#       name[7] = 'the'
#
# It'll return a Error Message that indicates the String data type does not supports assignments.

for i in name:
    print('* * * ' + i + ' * * *')
