"""
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1

    Description: The following program lets the user type in a pet name
    and then checks to see whether the name is in a list of pets.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"



myPets = ['Zophie', 'Pooka', 'Fat-tail']
print('Enter a pet name:')
name = input()
if name not in myPets:
    print('I do not have a pet named ' + name)
else:
    print(name + ' is my pet.')
