"""
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1

    Description: Working with lists in Python.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"

spam = [['cat', 'bat'], [10, 20, 30, 40, 50]]

# Using indices
# More: https://automatetheboringstuff.com/eval/13-1.html
#       https://automatetheboringstuff.com/eval/13-2.html
#       https://automatetheboringstuff.com/eval/13-4.html
#       https://automatetheboringstuff.com/chapter4/
print(spam[1][4])
print(spam[1][-1]) # Negative indices bring us an item coming backwards
print(spam[0][-1]) # Negative indices bring us an item coming backwards

# Using slices
print(spam[1][1:3]) #Returns a new part of list between 1 (included) and 3 (excluded) indices

# Assign new values
print(spam[0])
spam[0][0] = 'rat'
print(spam[0])

# Overriding new values from a slice part with a new list
print(spam[1])
spam[1][1:3] = ['CAT', 'MICE', 'DOG'] # Getting two values and replace them with three new ones
print(spam[1])                        # Yes, we're adding an extra one!

# Using Slice Shortcuts
# At the second index, we let a blank value before the colon.
# The same result for: spam[1][0:3]
# This says to Python something like:
#           - Ok, return for me a slice from beginning until third index
print(spam[1][:3])

# del Statement
# after deleting a value, python reorganizes the list. There isn't any gaps in a list.
del spam[1][2] #removing the index two (third position)
print(spam[1])

str_to_list = list('Hello Andre') #Making a list from String
print(str_to_list)

str_to_list[:6] = ['Hello'] #Replacing the slice from 0 until 6 (excluded) indices
print(str_to_list)

str_to_list[1:] = ['Andre'] #Replacing the slice from 6 until the end
print(str_to_list)

print('Andre' in str_to_list)
