"""
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1

    Description: Using lists, you can write a much more elegant version of the previous chapter’s Magic 8 Ball program.
    Instead of several lines of nearly identical elif statements, you can create a single list that the code works with.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"

import random

messages = ['It is certain',
    'It is decidedly so',
    'Yes definitely',
    'Reply hazy try again',
    'Ask again later',
    'Concentrate and ask again',
    'My reply is no',
    'Outlook not so good',
    'Very doubtful']

print(messages[random.randint(0, len(messages) - 1)])
