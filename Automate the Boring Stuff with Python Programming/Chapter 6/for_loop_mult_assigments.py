"""
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1

    Description: For loop with lists in Python.
                 Multiple assigments
                 Augmented Assignment Operators
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"
