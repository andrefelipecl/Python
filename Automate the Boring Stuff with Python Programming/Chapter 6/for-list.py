"""
    Date created: 07/11/2017
    Date last modified: 07/16/2017
    Python version: 3.6.1

    Description: Working with lists in Python.
"""
__author__ = "Andre Felipe Camargo Leite"
__email__ = "andrefelipecl@outlook.com"
__license__ = "GPL"


# Technically, a for loop repeats the code block once
# for each value in a list or list-like value. For example,
# if you ran this code
for i in range(4):
    print(i)

# The following program has the same output as the previous one:
for i in [0, 1, 2, 3]:
    print(i)

# For example, enter the following into the interactive shell:
supplies = ['pens', 'staplers', 'flame-throwers', 'binders']
for i in range(len(supplies)):
    print('Index ' + str(i) + ' in supplies is: ' + supplies[i])

# The in and not in Operators
print('howdy' in ['hello', 'hi', 'howdy', 'heyas'])

spam = ['hello', 'hi', 'howdy', 'heyas']
print('cat' in spam)

print('howdy' not in spam)

print('cat' not in spam)


#The remove() method is passed the value to be removed from the list it is called on.
spam = ['cat', 'bat', 'rat', 'elephant']
spam.remove('bat')
print(spam)

#f the value appears multiple times in the list, only the first instance of the value will be removed.
spam = ['cat', 'bat', 'rat', 'cat', 'hat', 'cat']
print(spam)
spam.remove('cat')
print(spam)

#The delete statement removes a value from a list passing the index
spam = ['cat', 'bat', 'rat', 'elephant']
del spam[1]
print(spam)

# Lists of number values or lists of strings can be sorted with the sort() method.
spam = [2, 5, 3.14, 1, -7]
spam.sort()
print(spam)

spam = ['ants', 'ac', 'ab', 'aa', 'cat', 'cats', 'dogs', 'ca', 'badgers', 'elephants']
spam.sort()
print(spam)

#you can also pass True for the reverse keyword argument to have sort()
#sort the values in reverse order.
spam = ['ants', 'ac', 'ab', 'aa', 'cat', 'cats', 'dogs', 'ca', 'badgers', 'elephants']
spam.sort(reverse=True)
print(spam)


#Third, sort() uses “ASCIIbetical order” rather than actual alphabetical
#order for sorting strings. This means uppercase letters come before lowercase letters.
spam = ['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
spam.sort()
print(spam)

# If you need to sort the values in regular alphabetical order, pass str.
# lower for the key keyword argument in the sort() method call.
spam = ['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
spam.sort(key=str.lower)
print(spam)
