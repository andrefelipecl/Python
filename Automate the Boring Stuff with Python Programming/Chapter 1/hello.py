'''
    Date created: 07/11/2017
    Date last modified: 07/11/2017
    Python version: 3.6.1
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

print('Hello World')
print('What is your name?') # ask for their name

# Some operations with their name
myname = input()
print('It is good to meet you, ' + myname)
print('The length of your name is: ' + str(len(myname)))

# Some operations with their age
print('What is your age?') # ask for their age
myage = input()
print('You will be ' + str(int(myage) + 1) + ' in a year.')

# Saying goodbye
print('GoodBye, ' + myname)
