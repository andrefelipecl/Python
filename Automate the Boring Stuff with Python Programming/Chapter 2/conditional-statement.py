'''
    Date created: 07/11/2017
    Date last modified: 07/11/2017
    Python version: 3.6.1
    Description:
        A simple example of IF, ELIF (ELSE IF) and ELSE statements (conditional statements), that show
        us the sintax, the use of the blocks on Python that its has indentations separating
        one block from another.
        Blocks help us in order to make us understand easily what part of block
        we are, and making the code cleaner.
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

EXPECTED_LOGIN = "andrefelipecl"
EXPECTED_PASSWORD = "135246"

print('Login: ')
login = input()

print('Password: ')
password = input()

    # Checking for empty values.
if not login or not password:
    print('Access denied!')

    # Checking for the right login and password
elif login == EXPECTED_LOGIN and password == EXPECTED_PASSWORD:
    print('Access granted!')
    print('Hi, Andre!')

    # otherwise...
else:
    print('Access denied!')
    
print('Done')
