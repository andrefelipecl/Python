'''
    Date created: 07/11/2017
    Date last modified: 07/11/2017
    Python version: 3.6.1
    Description: Gauss's Some until 100 and Python 'for loop'
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

total = 50
for x in range(1,51):
    a = 100 - x
    b = a + x
    total += b
    if(x != 50):
        print(str(x) +') '+ str(x) +' + '+ str(a) +' = '+ str(b) )
    else:
        print('... and then, summing the last number 50 to total we have got...')

print('The sum is: ' + str(total) )
