'''
    Date created: 07/12/2017
    Date last modified: 07/12/2017
    Python version: 3.6.1
    Description: This code is showing us how to create functions, the non-values
                 and keywords arguments of a function.
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

def hello(name ):
    print('Hello, ' + name)

hello('Andre')
print() #Printing a blank line

# overwriting the karg 'end' of print function
print('Using the keyword param \'end\' of print function.', end=' ')
print('This new print must be in the same line because the karg \'end\' was overwritten.')
print() #Printing a blank line

# overwriting the karg 'sep' of print function
print('mouse', 'rat', 'mice', sep=', ', end=': ')
print('These are names for similar animals in this world; \'Mice\' is the plural for them.')
