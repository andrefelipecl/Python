'''
    Date created: 07/11/2017
    Date last modified: 07/11/2017
    Python version: 3.6.1
    Description: This code is showing us the use of modules and their functions.
                 The 'pyperclip', third's module, module will help us with functions
                 of copy and paste to clipboard.

    Further information:
        To run this code in Python you need to intall some third modules on Python:
            - pyperclip (Command: 'pip install pyperclip')
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"

import sys, pyperclip

print('Hello!')

# try command paste from clipboard to some text editor and check it out...
pyperclip.copy('Pyperclip copied this texto to clipboard! Amazing...')

sys.exit()

#this below line won't displayed because the 'exit()' function will exit the program before.
print('GoodBye!')
