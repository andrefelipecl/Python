'''
    Date created: 07/11/2017
    Date last modified: 07/11/2017
    Python version: 3.6.1
    Description: Using and exploring try-exception statement in a code.
'''
__author__  = "Andre Felipe Camargo Leite"
__email__   = "andrefelipecl@outlook.com"
__license__ = "GPL"


def div24y(divideBy):
    try:   
        return 42 / divideBy
    # Avoiding Error by divide by zero.
    except ZeroDivisionError as zde:
        return 'Error: You tried to divide by zero.'

print(div24y(12))
print(div24y(6))
print(div24y(0))
print(div24y(84))
